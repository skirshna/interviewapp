using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FunCoding.UserDetails;

namespace FunCoding.Src.MultiThreading
{
    public class MergeLists
    {
        /* to read from 3 list in different thread and merge them */
        private static int GetMaxListSize(IEnumerable<IList<IUser>> getEnumerator)
        {
            var temp = getEnumerator.Select(t => t.Count).Concat(new[] {0}).Max();
            return temp;
        }

        public static IList<IUser> GetMergedUsers()
        {
            var userRepository = new UserRepository();

            var t1 = Task.Run(() => userRepository.GetNewUsers());
            var t2 = Task.Run(() => userRepository.GetPaidUsers());
            var t3 = Task.Run(() => userRepository.GetMatchedUsers());

            //main thread waits till all the user data is loaded from other threads
            t1.Wait();
            t2.Wait();
            t3.Wait();
            
            var AllUserLists = new List<IList<IUser>>();
            AllUserLists.Add(t1.Result);
            AllUserLists.Add(t2.Result);
            AllUserLists.Add(t3.Result);
            
            //if (userLists == null || userLists.Length is 0) throw new ArgumentNullException();

            return LinearMerge(AllUserLists);
        }

        private static IList<IUser> LinearMerge( IList<IList<IUser>> userLists)
        {
            var mergedList = new List<IUser>();
            for (var i = 0; i < GetMaxListSize(userLists); i++)
                mergedList.AddRange(from t in userLists where i < t.Count select t[i]);

            return mergedList;
        }
        
        
        public static void PrintOutput(IList<IUser> mergedList)
        {
            Console.WriteLine($"{"Id",20}: {"Name",-20}");
            Console.WriteLine($"{"=========",20}--{"==========",-20}");
            mergedList.ToList().ForEach(_user => Console.WriteLine($"{_user.Id,20}: {_user.Name,-20}"));
            Console.WriteLine($"{"=========",20}--{"==========",-20}");
            Console.WriteLine("\n");
            Console.WriteLine($"{"=========",20}--{"==========",-20}");
        }
    }
}