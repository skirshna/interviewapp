namespace FunCoding.UserDetails
{
    public interface IUser
    {
        string Name { get; set; }
        string Description { get; set; }
        string Id { get; set; }
    }

    public class User : IUser
    {
        public User(string name, string description, string id)
        {
            Name = name;
            Description = description;
            Id = id;
        }

        public string Name { get; set; }
        public string Description { get; set; }
        public string Id { get; set; }
        
        public override bool Equals(object obj) {
            return this.Id.Equals(((User)obj)?.Id);
        }

        public override int GetHashCode() {
            return this.Id.GetHashCode();
        }
    }
    
}