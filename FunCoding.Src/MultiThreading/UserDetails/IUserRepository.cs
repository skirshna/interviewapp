using System.Collections.Generic;

namespace FunCoding.UserDetails
{
    public interface IUserRepository
    {
        List<IUser> GetMatchedUsers();
        List<IUser> GetNewUsers();
        List<IUser> GetPaidUsers();
    }
}