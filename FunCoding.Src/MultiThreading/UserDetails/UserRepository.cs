using System.Collections.Generic;
using System.Threading.Tasks;

namespace FunCoding.UserDetails
{
    public class UserRepository : IUserRepository
    {
        public List<IUser> GetPaidUsers()
        {
            Task.Delay(5).Wait();
            return new List<IUser>
            {
                new User("Frank", "PaidUser", "15"),
                new User("Declan", "Hello Ladies", "20")
            };
        }

        public List<IUser> GetNewUsers()
        {
            Task.Delay(500).Wait();
            return new List<IUser>
            {
                new User("Siyavash", "Come here often?", "10"),
                new User("Reza", "SUP???", "290"),
                new User("Frank", "PaidUser", "15"),
                new User("André", "Comment ca-va?", "11"),
                new User("Gayane", "You know nothing", "91")
            };
        }

        public List<IUser> GetMatchedUsers()
        {
            Task.Delay(1500).Wait();
            return new List<IUser>
            {
                new User("Dryden", "I wrote this", "12"),
                new User("Ron", "Hel-lo", "92"),
                new User("Markus", "Who broke my site?", "3121"),
                new User("MHS", "I'm a boat", "392"),
                new User("James", "Let's bee friendz", "121"),
                new User("Frank", "PaidUser", "15"),
                new User("Chris", "Another Chris!", "192")
            };
        }
    }
}