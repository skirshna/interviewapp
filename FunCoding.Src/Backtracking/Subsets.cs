using System;
using System.Collections.Generic;

namespace FunCoding.Src.Backtracking
{
    public class Subsets
    {
        public IList<IList<int>> GetUniqueSubsets(int[] nums) {
            var subsets = new List<IList<int>>();
            backTrack(nums, 0, subsets, new List<int>());
            return subsets;
        }
    
        private void backTrack(int[] nums, int start, IList<IList<int>> subsets, IList<int> current) {
            subsets.Add(new List<int>(current));
            for(int i=start; i < nums.Length; ++i) {
          
                current.Add(nums[i]);
                backTrack(nums, i+1, subsets, current);
                current.Remove(nums[i]);
            }
        }
    
        private void print(IList<int> nums) {
            foreach (var x in nums) {
                Console.Write(x+"-");
            }
            Console.WriteLine("\n");
        } 
    }
}