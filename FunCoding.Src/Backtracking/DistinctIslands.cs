using System;
using System.Collections.Generic;
using System.Linq;

//https://leetcode.com/problems/number-of-distinct-islands/

namespace FunCoding.Src.Backtracking
{
    public class DistinctIslands
    {
        public int Count;

        public int NumDistinctIslands(int[][] grid)
        {
            List<Island> all = new List<Island>();

            for (var i = 0; i < grid.Length; i++)
            {
                for (var j = 0; j < grid[0].Length; j++)
                {
                    if (grid[i][j] == 1)
                    {
                        //Console.WriteLine(i + " -> " + j);
                        Count += 1;
                        var island = new Island();
                        backTrack(grid, i, j, island); //g,0,0,_,0
                        //print(grid);
                        all.Add(island);
                    }
                }
            }

            // return Count; - return all island

            var islandDiffs = GetIslandDiffs(all);
            return GetUniqueCount(islandDiffs);
        }

        private void backTrack(int[][] grid, int i, int j,
            Island current)
        {
            if (i < 0 || j < 0 || i >= grid.Length || j >= grid[i].Length
                || grid[i][j] != 1)
            {
                return;
            }

            var pair = Tuple.Create(i, j);
            current.Index.Add(pair);

            grid[i][j] = -1;

            backTrack(grid, i, j + 1, current);
            backTrack(grid, i + 1, j, current);
            backTrack(grid, i, j - 1, current);
            backTrack(grid, i - 1, j, current);
        }

        private bool ContainsWithSequenceEquals(List<List<Tuple<int, int>>> list,
            List<Tuple<int, int>> target)
        {
            for (var x = 0; x < list.Count; x++)
            {
                if (list[x].SequenceEqual(target))
                {
                    return true;
                }
            }

            return false;
        }

        //note: the hashset comparator doesn't work for <list<tuples>>.
        //so we have to write our own comparator.
        private int GetUniqueCount(List<List<Tuple<int, int>>> allDiffs)
        {
            var uniqueDiffs = new List<List<Tuple<int, int>>>();
            for (var x = 0; x < allDiffs.Count; x++)
            {
                if (!ContainsWithSequenceEquals(uniqueDiffs, allDiffs[x]))
                {
                    uniqueDiffs.Add(allDiffs[x]);
                }
            }

            return uniqueDiffs.Count;
        }
        
        private List<List<Tuple<int, int>>> GetIslandDiffs(List<Island> all)
        {
            // [22,11,12], [66,55,56] => [00 11 10 ], [00 11 10 ]
            var uniqueIsland = new List<List<Tuple<int, int>>>();
            foreach (var island in all)
            {
                var diff = new List<Tuple<int, int>>();

                diff.Add(Tuple.Create(0, 0));
                for (var m = 1; m < island.Index.Count; m++)
                {
                    var i = island.Index[m].Item1 - island.Index[0].Item1;
                    var j = island.Index[m].Item2 - island.Index[0].Item2;
                    var temp = Tuple.Create(i, j);
                    diff.Add(temp);
                }

                uniqueIsland.Add(diff);
            }

            return uniqueIsland;
        }


        private void Print(IReadOnlyList<int[]> grid)
        {
            foreach (var t in grid)
            {
                for (var j = 0; j < grid[0].Length; j++)
                {
                    Console.Write(t[j] + " ");
                }

                Console.WriteLine();
            }

            Console.WriteLine("-----");
        }
    }

    public class Island
    {
        public List<Tuple<int, int>> Index { get; set; }

        public Island()
        {
            Index = new List<Tuple<int, int>>();
        }
    }
}