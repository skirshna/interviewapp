using System;
using System.Collections.Generic;
using System.Text;

namespace FunCoding.Src.Backtracking
{
    public class LetterCasePermutation
    {
        public IList<string> GetLetterCasePermutation(string S) {
     
            var output = new List<string>();
            Recursive(S,0,new StringBuilder(),output);
            return output;  
        }  
    
        private void Recursive(String S, int i, StringBuilder combo, List<String> output) {
            if(i >= S.Length) { 
                output.Add(combo.ToString());
                return;
            } 

            if(!char.IsLetter(S[i])) {
                combo.Append(S[i]);
                Recursive(S, i+1, combo, output);
            
            } else {
                combo.Append(char.ToUpper(S[i]));
                Console.WriteLine("C->" +i + "-" + combo);
                Recursive(S, i+1, combo, output);
                var l = combo.Length;
                combo.Remove(i, l-i); // no of element to remove is [i l).
           
                combo.Append(char.ToLower(S[i])); 
                Recursive(S, i+1, combo, output);
            }
        } 
    }
}