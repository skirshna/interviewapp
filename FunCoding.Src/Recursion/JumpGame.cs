using System;
using System.Collections.Generic;
using System.Linq;

namespace FunCoding.Src.Recursion
{
    //https://leetcode.com/problems/jump-game-ii/
    //the state: remaining step left to reach the last index.
    public class JumpGame
    {
        Dictionary<int,int> map = new Dictionary<int,int>();
        
        public int Jump(int[] nums) {
            List<int> list =  nums.ToList();
            return GetMinSteps(list);
        }
      
        private int GetMinSteps(List<int> nums) { 
        
            if(map.ContainsKey(nums.Count)) {
                return map[nums.Count];
            }
        
            if(nums.Count ==1 ) {
                return 0; 
            }
        
            if(nums[0] == 0) {
                return -1;
            } 
        
            var first = nums[0]; 
            int val = -1;
              
            for(var i=1; i<= first && i < nums.Count; i++) {
                var op = GetMinSteps(nums.Skip(i).Take(nums.Count-i).ToList()); //i.e nums[1:]
                if(op == -1) {
                    continue;
                }
                var num = 1+ op;
                val = ((val == -1) ? num : Math.Min(val, num)); 
            }
        
            map[nums.Count] = val;
        
            return val;
        }
    }
}