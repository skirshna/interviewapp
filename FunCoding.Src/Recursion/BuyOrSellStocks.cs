using System;
using System.Collections.Generic;

namespace FunCoding.Src.Recursion
{
    public class BuyOrSellStocks {

    Dictionary<(int,bool),int> stateMap = new Dictionary<(int, bool), int>();
        int transactionFee = 0;
    
        public int MaxProfit(int[] prices, int fee) {
            transactionFee = fee;
            return Dp(prices,0,false);
        }
    
        private int Dp(int[] prices, int current, bool iHoldAStockCurrently) {
        
            if(stateMap.ContainsKey((current,iHoldAStockCurrently))) {
                return stateMap[(current,iHoldAStockCurrently)];
            }
           
            if(current == prices.Length-1) {
                if(iHoldAStockCurrently) {
                    return prices[current] - transactionFee;
                } else {
                    return 0;
                }
            }

            var doNothingProfit = 0+Dp(prices, current+1, iHoldAStockCurrently);
        
            var tradeProfit = 0;
            if(iHoldAStockCurrently) {
                var profit = (prices[current] - transactionFee);
                tradeProfit = Dp(prices, current+1,false) + profit;
            } else {
                var profit = -prices[current];
                tradeProfit = Dp(prices, current+1,true) + profit;   
            }
        
            var bestProfit = Math.Max(doNothingProfit, tradeProfit);
        
            stateMap[(current,iHoldAStockCurrently)] = bestProfit;
            return bestProfit;
        }
    }
}