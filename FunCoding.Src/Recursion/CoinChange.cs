using System;
using System.Collections.Generic;

namespace FunCoding.Src.Recursion
{
    //https://leetcode.com/problems/coin-change/
    public class CoinChangeDp
    {
        /* DP notes: question that we have to ask is:
        what's the state? in this case, it is how much amt is left
    
        Given amt - coin value = remaining is the state
        so, from the initials state -  recursively ask what's the min no 
        of coin required to make `remaining`*/

        Dictionary<int,int> map = new Dictionary<int,int>(); //amt,val
        public int CoinChange(int[] coins, int amt) {
            return Calc(coins, amt);
        } 
    
        private int Calc(int[] coins, int amt) {

            int val = -1; 
            //can keep -1 or int.max. int.max gets restrictive and we have to return -1 
            //if no ans is found
        
            if(amt == 0) { 
                return 0;
            }
        
            if(map.ContainsKey(amt)) {
                return map[amt];
            }
            
            /* val = -1
             for n in nums:
             val = f(val,num)
             */
        
            foreach (var coin in coins)
            {
                if(coin <= amt) {
                    var op =  Calc(coins, amt-coin);
                    if(op == -1) {
                        continue;
                    } else
                    {
                        var num = 1 + op;
                        val = ((val == -1) ? num : Math.Min(val, num)); 
                    }
                }
            }
        
            map[amt] = val;
            return val;
        }
    }
}