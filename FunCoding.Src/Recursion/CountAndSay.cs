using System.Text;

namespace FunCoding.Src.Recursion
{
    //https://leetcode.com/problems/count-and-say/
    public class CountAndSay
    {
        public string countAndSay(int n) {
            if(n==1) {
                return "1";
            }
        
            if(n==2) {
                return "11";
            }
        
            var recursion = countAndSay(n-1);
            var answer= ParseCurrent(recursion);
            return answer.ToString();

        }

        private static StringBuilder ParseCurrent(string recursion)
        {
            StringBuilder answer = new StringBuilder();

            var count = 1;
            var previous =int.Parse(recursion[0].ToString());
            int current = 0;
            for(var i= 1; i < recursion.Length; i++) {
                current = (int)(recursion[i] - '0');
                if(current == previous) {
                    count++;
                } else {
                    answer.Append($"{count}{previous}");
                    count=1;
                    previous = current;
                }
                //Console.WriteLine($" i: {i} + curr : {current} + p: {previous} + count : {count}");
            }
            
            if(current > 0) { 
                answer.Append($"{count}{current}");
            }

            return answer;
        }
    }
}