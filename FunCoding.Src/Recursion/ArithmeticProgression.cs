namespace FunCoding.Src
{
    public class ArithmeticProgression
    {
        int sum = 0;
    
        public int NumberOfArithmeticSlices(int[] A) {
            //return bruteForce(A);
            NumSeqThatEndAtI(A, A.Length-1);
            return sum;
        }
    
        private int bruteForce(int[] A) {
            if(A.Length < 3) {
                return 0;
            }
    
            var count = 0;
            for(var j=0; j<A.Length-2; j++) {
                for(var i=j; i< A.Length-2; i++) {
                    if((A[i]-A[i+1])==(A[i+1]-A[i+2])) {
                        count+=1;
                    }   else {
                        break;
                    }   
                } 
            }
        
            return count;
        }
    
        //O(n) time and space.
        //todo: DP: O(n) space.
        private int NumSeqThatEndAtI(int[] A, int i) { //starts from l-1
            if( i < 2) {
                return 0;
            }
       
            /*
             4,8,12,16,20
                        ^
             recursion1: ap:1; s:1
             recursion2: ap:1+1 = 2; s=3
             recursion3: ap:1+2=3; s=6
            */
        
            var ap = 0;
            if((A[i]-A[i-1])==(A[i-1]-A[i-2])) {
                ap = 1+NumSeqThatEndAtI(A, i-1);
                sum+=ap;
                //Console.WriteLine("ap: {0} ; sum: {1}; i: {2}",ap, sum, i);
            } else {
                NumSeqThatEndAtI(A,i-1);
            }
        
            return ap;
       
        }
    }
    
    
}