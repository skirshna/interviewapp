using System;
using System.Threading.Tasks;
using FunCoding.ArrayManipulations;
using FunCoding.Src.ArrayManipulations;
using FunCoding.Src.GraphTraversal;
using FunCoding.Src.MultiThreading;
using FunCoding.Src.StringManipulation;
using FunCoding.UserDetails;

namespace FunCoding.Main
{
    internal static class Program
    {
        public static void Main()
        {
            PrintMergedUser();
        }

        private static void PrintMergedUser()
        {
            MergeLists.PrintOutput(MergeLists.GetMergedUsers());
        }

        private static void FunCodingOutputs()
        {
            RomanNumeral.GetInstance().RomanNumeralToNumber("XIVV");
            ArrayManipulation.PlusOne(new int[] {1,2,3,5,6});
            GraphMain();
            Console.WriteLine(FunStringPlays.LongestSubstring("abcabcbb"));
        }

        private static void GraphMain()
        {
            var graph = Graph<int>.PopulateGraph();
            Console.WriteLine("\n DFS: ");
            var dfs = GraphTraversal<int>.DepthFirstSearch(graph,7);
            foreach (var i in dfs)
            {
                Console.WriteLine($"{i} -> ");
            }
            
            Console.WriteLine("\n BFS: "); 
            var bfs = GraphTraversal<int>.BreadthFirstSearch(graph,7);
            foreach (var i in bfs)
            {
                Console.WriteLine($"{i} -> ");
            }
        }
        


       
    }
}