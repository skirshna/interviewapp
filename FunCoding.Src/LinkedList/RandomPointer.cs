using System;
using System.Collections.Generic;

namespace FunCoding.Src.LinkedList
{
    public class Node
    {
        public Node Next;
        public Node Random;
        public int Val;

        public Node(int val)
        {
            Val = val;
            Next = null;
            Random = null;
        }
    }

    public class RandomPointer
    {
        private readonly Dictionary<int, Node> newOrder = new Dictionary<int, Node>(); //generated

        //can just map old node => new node.
        private readonly Dictionary<Node, int> originalOrder = new Dictionary<Node, int>(); // original

        private readonly Dictionary<Node, Node>
            RandomLink = new Dictionary<Node, Node>(); //original node, original random

        public Node CopyRandomList(Node head)
        {
            if (head == null) return head;

            PopulateRandomLinks(head);

            var newList = LinearList(head);
            return AssignRandom(head, newList);
        }

        private void PopulateRandomLinks(Node head)
        {
            var p = head;

            while (p != null)
            {
                try
                {
                    RandomLink.Add(p, p.Random);
                }
                catch (ArgumentException e)
                {
                  //ideally, check for not contains but might b
                  //end up accessing dictionary twice.
                }

                p = p.Next;
            }
        }

        private Node AssignRandom(Node head, Node newHead)
        {
            var p = head;
            var q = newHead;

            while (q != null)
            {
                //assumption: p and q have same count
                var oldRandom = RandomLink[p];
                if (oldRandom != null)
                {
                    var oldRandomOrder = originalOrder[oldRandom];
                    var newRandom = newOrder[oldRandomOrder];
                    q.Random = newRandom;
                }
                else
                {
                    q.Random = null;
                }

                q = q.Next;
                p = p.Next;
            }

            return newHead;
        }

        private Node LinearList(Node head)
        {
            var p = head;
            Node newHead = null;
            Node q = null;

            var count = 0;

            while (p != null)
            {
                if (newHead == null)
                {
                    newHead = new Node(p.Val);
                    newHead.Next = null;
                    q = newHead;
                }
                else
                {
                    var temp = new Node(p.Val);
                    temp.Next = null;
                    q.Next = temp;
                    q = q.Next;
                }

                originalOrder.Add(p, count); // node - key: order of the node it points to as value
                newOrder.Add(count, q); // // ordering of node, node
                count += 1;
                p = p.Next;
            }

            return newHead;
        }

        private void PrintList(Node h)
        {
            while (h != null)
            {
                Console.Write(h.Val + "-> ");
                h = h.Next;
            }

            Console.WriteLine();
        }
    }
}