using System.Collections.Generic;

namespace FunCoding.Src.LinkedList
{
    public class LinkedListCycle {
        public bool HasCycle(ListNode head) {
            var set = new HashSet<ListNode>();
            var  p = head;
            while (p != null) {
            
                if(set.Contains(p)) {
                    return true;
                }
                set.Add(p);
                p=p.next;
            }
        
            return false;
        }
    }
}