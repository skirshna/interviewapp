using System;

namespace FunCoding.Src
{
    public class ListNode {
        public int val;
        public ListNode next;
        public ListNode (int v, ListNode n) {
            val= v;
            next = n;
        }
    }
    
    /*
      2,4,5
          ^
      1,3,5
          ^
      2,6
        ^
      1,2,2,3,4
    */
    
    public class MergeSortedKList
    {    
        private int returnMin(ListNode[] p) {
            var minIndex = -1;
            var minValue = Int32.MaxValue;
            for(var i =0; i< p.Length; i++) {
                if (p[i] == null)
                {
                    continue;
                }

                if(p[i].val < minValue) {
                    minValue = p[i].val;
                    minIndex = i;
                }
            }
        
            Console.WriteLine($"min index : {minIndex}");

            return minIndex;
        }
    
        public ListNode MergeKLists(ListNode[] lists) {
            var count = lists.Length; 
            var output = new ListNode(0,null);
            var o = output;
            var p = new ListNode[count];
            for(var i=0; i<count;i++) {
                p[i] = lists[i];
            }

            var minIndex = returnMin(p);
        
            while(minIndex != -1) {
                var temp = new ListNode(p[minIndex].val,null);
                p[minIndex] = p[minIndex].next;
                o.next=temp;
                o=o.next;
                minIndex = returnMin(p);
            }
        
            return output.next;
        }
    }
}