using System;
using System.Collections.Generic;

namespace FunCoding.Src.LinkedList
{
    public class ListNode {
        public int val;
         public ListNode next;
         public ListNode(int val=0, ListNode next=null) {
             this.val = val;
             this.next = next;
         } 
    }
    
public class Solution {
     //TODO: modify this code to add carry over as you parse the list. 
     private ListNode ReverseLinkedList(ListNode head) {
         
       if(head == null) return null;
       ListNode reversedList = null;
       
       Stack<int> s = LinkedListToStack(head);
       if(s.Count == 0) return null;
         
       reversedList = insertIntoList(reversedList, s.Pop());
       ListNode l = reversedList;
       
        while(s.Count != 0) {
            var no = s.Pop();
            l = insertIntoList(l,no);
        }
        
        return reversedList;  
    }
    
    
    private Stack<int> LinkedListToStack(ListNode head) {
        ListNode l = head;
        Stack<int> s = new Stack<int>();
        while (l!=null) {
            s.Push(l.val);
            l=l.next;
        }
        return s;
    }
    
   
    private int extractNumbersFromList (ListNode head) {
        
        if(head == null) {
             return 0;
        } 
        
        ListNode l = head;
        
        int number = 0;
        int place = 1;
        
        while(l!= null) //2->3->4  => 432
        {
            int item = l.val;//4
            number=number+(item*place);
            place=place*10;
            l=l.next;
            
            Console.WriteLine("no -> " + number);
        }
        return number; //432
    }

    private ListNode insertIntoList( ListNode head, int no) {
        
        ListNode temp = new ListNode(no, null);
        
        if(head == null) {
            return temp;
        } else {
            head.next=temp;
        }
        
        return temp;
    }
    
    private ListNode generateLinkedListFromNo(int no)
    {
        ListNode head = null;
        ListNode l = null;
        bool isFirstVisited = false;
        while(no != 0) {
            var r=no%10;
            no = no /10;

            if (!isFirstVisited)
            {
                isFirstVisited = true;
            }
            
            if (isFirstVisited)
            { 
                head = insertIntoList(head,no);
                l = head;
            } else {
                l= insertIntoList(l,no);
            }
        }
        
        return head;
    }
    
    public ListNode AddTwoNumbers(ListNode l1, ListNode l2) {
        
        if(l1 == null && l2 == null) return null;
        
        if(l1 == null) return ReverseLinkedList(l2);
        
        if(l2 == null) return ReverseLinkedList(l1);
    
        int firstNo = extractNumbersFromList(l1);
                       //o(n)
        
        Console.WriteLine("reversing first linked list -> " + firstNo);  
          
        int secondNo = extractNumbersFromList(l2);
                        //0(n)
        Console.WriteLine("reversing second linked list -> " + secondNo);
        
        return ReverseLinkedList(generateLinkedListFromNo(firstNo+secondNo)); 
    }
}
}