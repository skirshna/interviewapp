using System;
using System.Collections.Generic;

namespace FunCoding.Src.ArrayManipulations
{
    public class SingleRowKeyboard
    {
        public int CalculateTime(string k, string word) {
            var position = new Dictionary<char,int>();
        
            for(var i=0; i< k.Length; i++) {
                position.Add(k[i],i);
            }
        
            var sum = position[word[0]];
            var last = position[word[0]];
            foreach(var w in word.Substring(1)) {
                sum += Math.Abs(last-position[w]);
                last= position[w];
            }
        
            return sum;
        }
    }
}