 
using System;
using System.Linq;
using System.Numerics;

namespace FunCoding.ArrayManipulations
{
    public class ArrayManipulation
    {
        //consider array as number. output needs to be +1 of the number.
        public static int[] PlusOne(int[] array)
        {
            if (array.Length == 0)
            {
                return new[] {1};
            }

            if (array[^1] != 9)
            {
                array[^1]++;
                return array;
            }

            return PlusOne(array.Take(array.Length - 1).ToArray()).Concat(new[] {0}).ToArray();
        }

        //consider array as number. output needs to be +1 of the number.
        public int[] PlusOneAnotherApproach(int[] array)
        {
            var input = string.Join("", array.Select(x => x.ToString()));
            var inputNo = BigInteger.Parse(input);
            inputNo += 1;
            var output = inputNo.ToString().ToCharArray();
            var outputNo = new int[output.Length];
            for (var i = 0; i < output.Length; i++)
            {
                outputNo[i] = Int32.Parse(output[i].ToString());
            }

            return outputNo;

        }

        private int ReturnSqrt(int x)
        {
            var result = Math.Sqrt(x);
            return Convert.ToInt32(Math.Floor(result));
        }
    }
}