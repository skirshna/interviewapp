using System;
using System.Collections.Generic;

namespace FunCoding.Src.ArrayManipulations
{
    // Longest Harmonious Subsequence - O(n)
    public class LongestHarmoniousArray {
        
        public int FindLHS(int[] nums) {
            var store = new Dictionary<int,int>();
            populate(store, nums);
            return GetMaxLength(store, nums);
        }
    
        private static void populate(Dictionary<int,int> store, int[] nums) {
            foreach(var no in nums) {
                try {
                    store[no]+=1;
                } catch(KeyNotFoundException _) {
                    store.Add(no,1);
                } 
           
            }
        }
    
        private static int GetMaxLength(IReadOnlyDictionary<int, int> store, int [] nums) {
            var max = 0;
            foreach(var no in nums) {
                try{
                    max= Math.Max(max, store[no+1]+store[no]);
                    max= Math.Max(max, store[no-1]+store[no]);
                } catch(KeyNotFoundException _) {
                    continue;
                } 
            }
        
            return max;
        }
    }
}