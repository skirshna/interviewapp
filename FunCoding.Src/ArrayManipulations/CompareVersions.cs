using System;

namespace FunCoding.Src.ArrayManipulations
{
    /*
     Input: version1 = "7.5.2.4", version2 = "7.5.3"
      Output: -1
     */
    public class CompareVersions
    {
        
        public int CompareVersion(string version1, string version2) {
            var v1 = version1.Split('.');
            var v2 = version2.Split('.');

            var i = 0;
            var v1l = v1.Length; 
            var v2l = v2.Length;
        
            int maxlen = Math.Max(v1l,v2l);
        
            while(i < maxlen) {
                var t1 = i < v1l? Int32.Parse(v1[i]) : 0;
                var t2 = i < v2l? Int32.Parse(v2[i]) : 0;
                //note: missed the edge case of 1.0.1 and 1

                if(t1 < t2) {
                    return -1;
                } else if (t1 > t2) {
                    return 1;
                }
            
                i++;
            }

            return 0;
        }
    }
}