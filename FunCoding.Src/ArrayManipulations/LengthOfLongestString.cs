using System;
using System.Collections.Generic;
using System.Collections.Immutable;
using System.Linq;

namespace FunCoding.Src.ArrayManipulations
{
    public class LengthOfLongestString
    {
        public static int LengthOfTheLongestString(string s)
        {
            int longestLength = 0;
            var i = 0;
            var oldIndex = 0;
            HashSet<char> visited = new HashSet<char>();
            while (i < s.Length)
            {
                var key = s[i];
                if (visited.Contains(key))
                {
                    longestLength = Math.Max(longestLength, visited.Count);
                    visited.Clear();
                    i = oldIndex++;
                }
                else
                {
                    visited.Add(key);
                    i++;
                }
            }

            return Math.Max(longestLength, visited.Count);
        }
    }
}



