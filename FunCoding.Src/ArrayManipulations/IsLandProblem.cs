namespace FunCoding.Src.ArrayManipulations
{
    public class IsLandProblem
    {
        public int NumIslands(char[][] grid) {
            int noOfIsland = 0;
            for(var i=0; i < grid.Length; i++) {
                for (var j = 0; j < grid[0].Length; j++) {
                    if(grid[i][j] == '1') {
                        visitIsland(grid,i,j);
                        noOfIsland++;
                   
                    }
                }
            }
        
            return noOfIsland;
        }
    
        private void visitIsland (char[][] grid, int i, int j) {
        
            if(i < 0 || i >= grid.Length || j < 0 || j >= grid[0].Length) {
                return;
            }
        
            if(grid[i][j] != '1') return;
        
        
            grid[i][j] = '*';
        
        
            visitIsland(grid,i+1,j);
            visitIsland(grid,i-1,j);
            visitIsland(grid,i,j+1);
            visitIsland(grid,i,j-1);
        }
    }
}