using System;
using System.Collections.Generic;

namespace FunCoding.Src.ArrayManipulations
{
    public class ShortestDistanceToAChar
    {
        public int[] GetShortestToChar(string s, char c)
        {
            var indices = GetIndicesOfACharacter(s, c);
            var output = new int[s.Length];
        
            for(var i=0; i<s.Length; i++) { 
                if(s[i]==c){
                    output[i]=0;
                }
                else {
                    var minDiff = int.MaxValue;
                    foreach(var index in indices) {
                        minDiff = Math.Min(minDiff, Math.Abs(index-i));
                    } 
                    output[i]=minDiff;
                }  
            }
            return output;
        }

        private List<int> GetIndicesOfACharacter(string s, char c)
        {
            var indices = new List<int>();
            for (var i = 0; i < s.Length; i++)
            {
                if (s[i] == c)
                {
                    indices.Add(i);
                }
            }

            return indices;
        }
    }
}