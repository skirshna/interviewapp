using System;
using System.Collections.Generic;
using System.Linq;

namespace FunCoding.Src
{
    public class  NSumProblem {
    
        Stack<int> visited;
        IList<IList<int>> output;
        private int[] inputArray;
    
        public NSumProblem() {
            visited = new Stack<int>();
            output = new List<IList<int>>();
        }
    
        public IList<IList<int>> FourSum(int[] nums, int target)
        {
           
            if (nums == null || nums.Length == 0)
            { 
                output.Add(new List<int>(){0,0,0,0});
                return output;
            }
          
            inputArray = nums;
            Array.Sort(inputArray);
            NSum(target, 4, 0);
            return output;
        }
        
        public IList<IList<int>> TwoSum(int[] nums, int target)
        {
           
            if (nums == null || nums.Length == 0)
            { 
                output.Add(new List<int>(){0,0});
                return output;
            }
          
            inputArray = nums;
            Array.Sort(inputArray);
            NSum(target, 2, 0);
            return output;
        }
        
        public IList<IList<int>> ThreeSum(int[] nums, int target)
        {
           
            if (nums == null || nums.Length == 0)
            { 
                output.Add(new List<int>(){0,0,0});
                return output;
            }
          
            inputArray = nums.ToHashSet().ToArray();
            Array.Sort(inputArray);
            NSum(target, 3, 0);
            return output;
        }
        
     
        private void NSum(int target, int k, int at) {
        
            //if we reached k elements and target is 0
            //then we print the stack.
            if(target == 0 && k==0) {
                printStack();
                return;
            }

            if (k == 0 || at == inputArray.Length)
            {
                return;
            }
            
            // 50   //5
            //a,b,c,d,e,f  //k=5   
            //50-a         //k=4
            //50-a-b       //k=3
            if(inputArray[at] <= target) {
                visited.Push(inputArray[at]);
                NSum(target-inputArray[at], k-1, at+1);
                visited.Pop();
            } 
            
            NSum(target, k, at+1);
            
        }
    
        private void printStack()
        {
            var list = visited.ToList();
            list.Reverse();
            output.Add(list);
        }
        
        private List<List<int>> ThreeSum(int[] nums)
        {
            Array.Sort(nums);
            
            var output = new List<List<int>>();
            var visited = new Dictionary<int, int>();

            for(var i=0; i <nums.Length-2; i++) {
                var target = nums[i]; //newTarget = target-a[i]
                var sub = 0;
                for(var j=i+1; j <nums.Length; j++) {
                    sub = -(nums[j]+target); //if(s.contains(newTarget-a[j]) //orubt
                    //s.add[a[j])
                    if((visited.ContainsKey(sub)) && !visited.ContainsKey(nums[j]))
                    {
                        var temp = new List<int>() {target, sub, nums[j]};
                        if(temp.ToHashSet().Count ==3 )
                            output.Add(temp);
                    }
                    visited[nums[j]] = j;
                }
            }
            
        
            return output;
        }
    }
}