using System.Collections.Generic;
using System.Linq;

namespace FunCoding.Src.ArrayManipulations
{
    public class RomanNumeral
    {
        //singleton object
        private static RomanNumeral _romanNumeral;
        
        private Dictionary<char, int> romanTable;
        
        private RomanNumeral()
        {
            romanTable = new Dictionary<char, int>
            {
                {'I', 1},
                {'V', 5},
                {'X', 10},
                {'L', 50},
                {'C', 100},
                {'D', 500},
                {'M', 1000}
            };

        }
        public static RomanNumeral GetInstance()
        {
            if (_romanNumeral == null)
            {
                _romanNumeral = new RomanNumeral();
            }

            return _romanNumeral;
        }
        public int RomanNumeralToNumber(string input)
        {
            var roman = input.ToCharArray();
            if (roman.Any(c => !romanTable.ContainsKey(c)))
            {
                return -1;
            }

            var result = 0;
            for (var i = 0; i < roman.Length; i++)
            {
                if (i+1 >= roman.Length)
                {
                    result += romanTable[roman[i]];
                }
                else if (romanTable[roman[i]] >= romanTable[roman[i + 1]])
                {
                    result += romanTable[roman[i]];
                } else if (romanTable[roman[i]] < romanTable[roman[i + 1]])
                {
                    result -= romanTable[roman[i]];
                }
            }

            return result;
        }
    }
}