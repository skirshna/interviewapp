using System;
using System.Collections.Generic;

namespace FunCoding.Src.GraphTraversal
{
    public class Graph<T>
    {
        private Dictionary<T, HashSet<T>>  AdjacencyList;

        public Graph(IEnumerable<T> vertices, IEnumerable<Tuple<T,T>> edges)
        {
            AdjacencyList = new Dictionary<T, HashSet<T>> ();
        
            foreach (var vertex in vertices)
            {
                AddVertex(vertex);
            }

            foreach (var edge in edges)
            {
                AddEdges(edge);
            }
            
        }

        private void AddVertex(T vertex)
        {
            AdjacencyList[vertex] = new HashSet<T>();
        }

        private void AddEdges(Tuple<T, T> edges)
        {
            if (AdjacencyList.ContainsKey(edges.Item1) && AdjacencyList.ContainsKey(edges.Item2))
            {
                AdjacencyList[edges.Item1].Add(edges.Item2);
                AdjacencyList[edges.Item2].Add(edges.Item1);
            }
        }
        
        public HashSet<T> GetNeighbors(T vertex)
        {
            return AdjacencyList[vertex];
        }
        
        public static Graph<int> PopulateGraph()
        {
            var vertices = new[] {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
            var edges = new[]{Tuple.Create(1,2), Tuple.Create(1,3),
                Tuple.Create(2,4), Tuple.Create(3,5), Tuple.Create(3,6),
                Tuple.Create(4,7), Tuple.Create(5,7), Tuple.Create(5,8),
                Tuple.Create(5,6), Tuple.Create(8,9), Tuple.Create(9,10)};

            var graph = new Graph<int>(vertices, edges);
            return graph;
        }
    }
}