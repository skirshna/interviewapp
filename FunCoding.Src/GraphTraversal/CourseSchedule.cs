using System;
using System.Collections.Generic;
using System.Linq;

namespace FunCoding.Src.GraphTraversal
{

//to check if the course schedule pre-requisite are not cyclic
//https://leetcode.com/problems/course-schedule/

    public class CourseSchedule
    {

        public Dictionary<int, HashSet<int>> graph;

        public bool CanFinish(int numCourses, int[][] prerequisites)
        {

            if (prerequisites.Length == 0)
            {
                return true;
            }

            graph = constructAM(prerequisites);
            return IsACyclic(graph, prerequisites[0][0]);
        }

        private Dictionary<int, HashSet<int>> constructAM(int[][] prerequisites)
        {

            var graph = new Dictionary<int, HashSet<int>>();

            for (var i = 0; i < prerequisites.Length; i++)
            {
                int key = prerequisites[i][0];

                Console.WriteLine("key->" + key);
                HashSet<int> set = new HashSet<int>();

                for (var j = 1; j < prerequisites[i].Length; j++)
                {
                    set.Add(prerequisites[i][j]);
                    Console.WriteLine("->" + prerequisites[i][j]);
                }

                if (graph.ContainsKey(key))
                {
                    var existingSet = graph[key];
                    existingSet.UnionWith(set);
                    graph[key] = existingSet;
                }
                else
                {
                    graph.Add(key, set);
                }
            }

            return graph;
        }


        private bool IsACyclic(Dictionary<int, HashSet<int>> graph, int start)
        {
            var visited = new HashSet<int>();
            var stack = new Stack<int>();

            stack.Push(start);
            Console.WriteLine("start->" + start);

            while (stack.Count != 0)
            {
                var node = stack.Pop();
                visited.Add(node);

                HashSet<int> neighbors;
                if (graph.ContainsKey(node))
                    neighbors = graph[node];
                else
                    continue;

                foreach (var n in neighbors)
                {
                    if (visited.Contains(n))
                    {
                        return false;
                    }
                    else
                    {
                        stack.Push(n);
                    }
                }
            }

            return true;
        }


        //won't work coz we need to find acyclic graph.
        public class InitialIdea
        {

            public bool ToExecute(int numCourses, int[][] prerequisites)
            {
                return CheckCount(numCourses, prerequisites) && validatePrerequisites(prerequisites);
            }

            private static bool CheckCount(int noCourses, int[][] courseList)
            {
                foreach (var list in courseList)
                {
                    if (!(list.Length <= noCourses))
                    {
                        return false;
                    }
                }

                return true;
            }


            private static bool validatePrerequisites(int[][] courseList)
            {
                for (var i = 0; i < courseList.Length - 1; i++)
                {
                    for (var j = 1; j < courseList.Length; j++)
                    {

                        var reverseFirst = new int[courseList[i].Length];
                        Array.Copy(courseList[i], reverseFirst, courseList[i].Length);
                        Array.Reverse(reverseFirst);

                        if (reverseFirst.SequenceEqual(courseList[j]))
                        {
                            return false;
                        }
                    }
                }

                return true;
            }
        }
    }
}