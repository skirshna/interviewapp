using System.Collections.Generic;

namespace FunCoding.Src.GraphTraversal
{
    public class GraphTraversal<T>
    {
        public static HashSet<T> DepthFirstSearch(Graph<T> graph, T start)
        {
            Stack<T> stack = new Stack<T>();
            HashSet<T> seen = new HashSet<T>();
            
            stack.Push(start);

            while (stack.Count != 0)
            {
                var currentNode = stack.Pop();
                if (seen.Contains(currentNode)) continue;
                seen.Add(currentNode);
               
                var neighbors = graph.GetNeighbors(currentNode);
                foreach (var node in neighbors)
                {
                    stack.Push(node);
                }

            }
            return seen;
        }
        
        public static HashSet<T> BreadthFirstSearch(Graph<T> graph, T start)
        {
            Queue<T> queue = new Queue<T>();
            HashSet<T> seen = new HashSet<T>();
            
            queue.Enqueue(start);

            while (queue.Count != 0)
            {
                var currentNode = queue.Dequeue();
                if (seen.Contains(currentNode)) continue;
                seen.Add(currentNode);
                var neighbors = graph.GetNeighbors(currentNode);
                foreach (var node in neighbors)
                {
                    queue.Enqueue(node);
                }

            }
            return seen;
        }
        
        public static void SpanningTree(Graph<T> graph, T start)
        {
            //TODO
        }
        
        public static void TopologicalSorting(Graph<T> graph, T start)
        {
            //TODO
        }
        
    }
}