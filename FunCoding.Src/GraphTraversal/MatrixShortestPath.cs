using System;
using System.Collections.Generic;

namespace FunCoding.Src.GraphTraversal
{
    //leetcode daily challenge : Shortest Path in Binary Matrix
    internal class NodeProperties {
        public int i;
        public int j;
        public int dist;
        
        public NodeProperties(int _x, int _y, int _dist) {
            i= _x;
            j= _y;
            dist = _dist;
        }
    }
    
    public class MatrixShortestPath
    {
        public int ShortestPathBinaryMatrix(int[][] grid) {
            if(grid[0][0] != 0 || grid[grid.Length-1][grid[0].Length-1] != 0) {
                return -1;
            }
        
            var queue = new Queue<NodeProperties>();

            queue.Enqueue(new NodeProperties(0,0,1));
        
            while(queue.Count > 0) {
                var qp = queue.Dequeue();
                if((qp.i,qp.j) == (grid.Length-1, grid[qp.i].Length-1))
                {
                    return qp.dist;
                }
                
                AddRelevantNeighbors(grid, qp, queue);
            }

            return -1;
        }
    
        private static void AddRelevantNeighbors(int[][] grid, NodeProperties current,
            Queue<NodeProperties> queue) {
        
            //check for all 8 neighbors and add them.
            var i=current.i;
            var j=current.j;
        
            for(var x=i-1;x<=i+1;x++) {
                for(var y=j-1;y<=j+1;y++) {
                    try
                    {
                        if (grid[x][y] != 0) continue;
                        
                        queue.Enqueue(new NodeProperties(x,y,current.dist+1));
                        grid[x][y]= -1;
                    } catch(IndexOutOfRangeException e) {
                        continue; //or check for max and min bound and continue;
                    }
                }
            }    
        }
    }
}