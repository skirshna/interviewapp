using System.Collections.Generic;
using System.Linq;

namespace FunCoding.Src.StringManipulation
{
    public class MinimumDeletion
    {
        //https://leetcode.com/problems/minimum-deletions-to-make-character-frequencies-unique/
        /*
        aaabbbcc
        map: 
        a - 3
        b - 3
        c - 2 
        
        <3,2,1> - count
        return 2
        */
    
        private static Dictionary<char,int> PopulateMap (string s) {
            var map = new Dictionary<char,int>();

            foreach(var x in s) {
                try{
                    map[x] +=1;
                } catch (KeyNotFoundException _) {
                    map.Add(x,1);
                }  
            }
        
            return map;
        }
    
        public int MinDeletions(string s) {
            var minChange = 0;
            var map = PopulateMap(s);
            var count = new HashSet<int>();
        
            var keys =  map.Keys.ToList();
        
            foreach(var key in keys) {
                while(count.Contains(map[key])) {
                    if(map[key] == 0) {
                        break;
                    }
                    minChange++;
                    map[key]-=1;
                }
                count.Add(map[key]);
            }
            return minChange;
        }
    }
}