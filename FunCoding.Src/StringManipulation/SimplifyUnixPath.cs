using System.Collections.Generic;
using System.Linq;

namespace FunCoding.Src
{
    public class SimplifyUnixPath
    {
        //python: use double ended queue. C# - implement a dequeue class and use it.
        public string GetSimplifiedPath(string path) {
            var stack = Dissect(path);
        
            return GetSimplifiedPath (stack);
       
        }
    
        private static string GetSimplifiedPath(Stack<string> _stack) {
        
            var stack = _stack.ToList();
        
            if (stack.Count == 0)
                return "/";
        
            var output = "";
            for(var i=stack.Count-1; i >=0; i--) {
                output+="/"+stack[i];
            }
        
            return output;   
        }
    
        private static Stack<string> Dissect(string path) {
            var subPaths = path.Split('/');
            var stack = new Stack<string>();
        
            foreach (var subPath in subPaths)
            {
                switch (subPath)
                {
                    case ".." when stack.Count > 0:
                        stack.Pop();
                        break;
                    case "":
                    case ".":
                    case ".." when stack.Count <= 0:
                        continue;
                    default:
                        stack.Push(subPath);
                        break;
                }
            }
            return stack;
        }
    }
}