using System.Collections.Generic;

namespace FunCoding.Src.StringManipulation
{
    //https://leetcode.com/problems/word-ladder/
    public class WordLadder
    {
        public int LadderLength(string beginWord, string endWord, IList<string> wordList) {
            return Bfs(beginWord, endWord, wordList);
        }
    
        private int Bfs (string start, string end, IList<string> list) {
            Queue<string> bfsQ = new Queue<string>();
            Dictionary<string, int> countToReachEnd = new Dictionary<string,int>();
        
            bfsQ.Enqueue(start);
            countToReachEnd.Add(start,1);
        
            while(bfsQ.Count != 0) {
            
                var node = bfsQ.Dequeue();
                foreach(var word in list) {
                    if(node.Equals(end)) {
                        return countToReachEnd[node];
                    }
                
                    if(IsOneLetterDifference(node, word) && !countToReachEnd.ContainsKey(word)) {
                        bfsQ.Enqueue(word);
                        countToReachEnd.Add(word, countToReachEnd[node]+1);
                    }
                }
            }
        
            return 0;
        }
    
        private bool IsOneLetterDifference(string current, string word) {
        
            if(word.Length != current.Length || word.Equals(current) ) {
                return false;
            }

            var count = 0;
            for(var i=0; i< word.Length; i++) {
                if(word[i] == current[i]) {
                    continue; 
                } else {
                    count++;
                }

                if(count >1) {
                    return false;
                }
            }
            
            return true;
        }
    }
}