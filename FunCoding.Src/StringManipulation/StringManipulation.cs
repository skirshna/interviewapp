using System;
using System.Collections.Generic;

namespace FunCoding.Src.StringManipulation
{
    public class FunStringPlays
    {
        public void ReverseString(char[] s)
        {
            if (s.Length != 0)
            {
                var start = 0;
                var end = s.Length - 1;
               
                while (start < end)
                {
                    var temp = s[end];
                    s[end] = s[start];
                    s[start] = temp;
                    end--;
                    start++;
                }
            }
        }
        
        //longest substring count without repeating characters
        public static int LongestSubstring(string s)
        {
            if (s == null) return 0;
            int maxCount = 0, i = 0, j = 0;
            var set = new HashSet<char>();

            while (j < s.Length)
            {
               var c = s[j];
               if (set.Contains(c))
               {
                   maxCount =  ReturnMaxCount(maxCount, set.Count);
                   j = i++;
                   set.Clear();
               }
               else
               {
                   set.Add(c);
                   j++;
               }
            }

            return ReturnMaxCount(maxCount, set.Count);
        }

        private static int ReturnMaxCount(int count1, int count2)
        {
            return Math.Max(count1, count2);
        }
    }
}