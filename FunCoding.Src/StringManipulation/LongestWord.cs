using System.Collections.Generic;
using System.Linq;

namespace FunCoding.Src.StringManipulation
{
    public class LongestWord
    {
        public string FindLongestWord(string str, IList<string> targets) {
        
            var words = targets.OrderByDescending(p => p.Length)
                .ThenBy(p=> p.Substring(0)).ToList(); 
        
            foreach (var target in words)
            {
                if(Recursive(str,target)) {
                    return target;
                }
            }
        
            return "";
        }
    
        private bool Recursive(string str, string target) {
        
            if(target == "") { 
                return true;
            }
        
            if(str == "") {
                return false;   
            }
        
            if(str[0]==target[0]) {
                return Recursive(str.Substring(1), target.Substring(1)); 
            } 
            
            return Recursive(str.Substring(1), target);
            //note: substring of index Length returns empty string in C#
        }
    
        private bool Iterative(string str, string target) {
            var p = 0; 
            var q =0;
            while(p < str.Length && q < target.Length) {
                if(str[p]==target[q]) {
                    p++;
                    q++;
                } else {
                    p++;
                }
            }
            
            return q==target.Length;
        }
    }
}