using System;
using System.Collections.Generic;
using System.Linq;

namespace FunCoding.Src.MinSwaps
{
    public class MinSwaps
    {
        /*
         * Questions: Return min steps to make a string palindrome.
         *
         * Approach:
         * Min step can be seen as shortest distance to go from string A -> string B, where B is palindrome
         * So every modified string can be nodes in the graph, and each swaps can be edges.
         * 
         * isValidate is to make sure the string can get palindrome - so we dont end up doing a expensive bfs for an
         * invalid string
         * 
         * ppqq -> pqpq -> pqqp
         */

        public static int MinSwapsForPalindrome(string s)
        {

            if (IsPalindrome(s))
            {
                Console.WriteLine("already a palindrome");
                return 0;
            }
            
            if (!IsValidate(s))
            {
                Console.WriteLine("invalid string for palindrome");
                return -1;
            }

            return GetMinSteps(s);
        }
        
        private static int GetMinSteps(string s)
        {
            Queue<string> bfsQ = new Queue<string>();
            Dictionary<string, int> visited = new Dictionary<string, int>();
            
            bfsQ.Enqueue(s);
            visited.Add(s,0);
            
            while (bfsQ.Count != 0)
            {
                var node = bfsQ.Dequeue();
                Console.WriteLine($"current node -> {node}");
                for (var i = 1; i < node.Length; i++)
                {
                    var nextNode = Swap(i - 1, i, node);
                    Console.WriteLine($"next node -> {nextNode}");
                    if (visited.ContainsKey(nextNode))
                    {
                        continue;
                    }
                   
                    if (IsPalindrome(nextNode))
                    {
                        return visited[node] + 1;
                    }
                    bfsQ.Enqueue(nextNode);
                    visited.Add(nextNode,visited[node]+1);
                }
            }

            return -1;
        }
        
        private static bool IsValidate(string s)
        {
            //for palindrome: count of each character needs to be count%2 = 0, except for one character;
            Dictionary<char, int> map = new Dictionary<char, int>();
            foreach (var x in s)
            {
                try
                {
                    map[x] += 1;
                }
                catch (KeyNotFoundException _)
                {
                    map.Add(x,1);
                }
            }

            var keyList = map.Keys.ToList();
            var allowedOdds = 1;
            var currentOdds = 0;
            foreach (var key in keyList)
            {
                if (map[key] % 2 == 0)
                {
                    continue;
                }
                else
                {
                    currentOdds += 1;
                    if (currentOdds > allowedOdds)
                    {
                        return false;
                    }
                }
            }

            return true;
        }

        private static bool IsPalindrome(string s)
        {
            var i = 0;
            var j = s.Length - 1;
            while (i < j)
            {
                if (s[i] != s[j])
                {
                    return false;
                }

                i++;
                j--;
            }

            return true;
        }
        
        private static string Swap(int i, int j, string s)
        {
            var charI = s[i];
            var charJ = s[j];
            var charArray = s.ToCharArray();
            charArray[i] = charJ;
            charArray[j] = charI;
            return new string(charArray);
        }
    }
}