using System.Collections.Generic;

namespace FunCoding.Src.Trees
{
    public class AverageOfALevel
    {
        
        IList<double> sum = new List<double>();
        IList<double> n = new List<double>();
        
        public IList<double> AverageOfLevels(TreeNode root) {
            IList<double> avg = new List<double>();
            Average(root, 0); 
        
            for(var i=0; i< sum.Count; i++) {
                avg.Add(sum[i]/n[i]);
            }
        
            return avg;
        }

        private void Average(TreeNode root, int level) {
            if(root == null) {
                return;
            }
        
            if(sum.Count-1 >= level) {
                sum[level] += root.val;
                n[level] += 1;
            } else {
                sum.Add(root.val);
                n.Add(1);
            }
        
            Average(root.left, level+1);
            Average(root.right, level+1);
        }
    }
}