namespace FunCoding.Src.Trees
{
    public class AddALevel
    {
        //https://leetcode.com/problems/add-one-row-to-tree/
        public TreeNode AddOneRow(TreeNode root, int val, int depth)
        {
            return FindAndInsertLevel(root, val, depth, 1);
        }

        private static TreeNode FindAndInsertLevel(TreeNode root, int val, int depth, int current)
        {
            if (root == null) return null;

            if (depth == 1)
            {
                var newRoot = new TreeNode(val);
                newRoot.left = root;
                return newRoot;
            }

            if (depth - 1 == current)
            {
                InsertLevel(root, val);
            }
            else
            {
                FindAndInsertLevel(root.left, val, depth, current + 1);
                FindAndInsertLevel(root.right, val, depth, current + 1);
            }

            return root;
        }

        private static void InsertLevel(TreeNode root, int val)
        {
            var left = new TreeNode(val);
            var right = new TreeNode(val);
            left.left = root.left;
            right.right = root.right;
            root.left = left;
            root.right = right;
        }
    }
}