namespace FunCoding.Src.Trees
{
    public class TrimBst
    {
        //trim the tree for the given limit
        //note - solve it for a given subtree and recurse.
        public TreeNode GetTrimmedBst(TreeNode root, int low, int high) {
            if (root == null) {
                return null;
            }
        
            if(root.val < low) {
                return GetTrimmedBst(root.right, low, high);
            } else if (root.val > high) {
                return GetTrimmedBst(root.left, low, high);
            } else {
                root.right = GetTrimmedBst(root.right, low, high);
                root.left = GetTrimmedBst(root.left, low, high);
            }
        
            return root;
        }
    }
}