using FluentAssertions;
using FunCoding.Src.StringManipulation;
using FunCoding.Src.MinSwaps;
using Xunit;

namespace FunCoding.Test.StringManipulationTest
{
    public class StringManipulationTest
    {
        [Theory]
        [InlineData("abcabcbb", 3)]
        [InlineData("bbbb", 1)]
        [InlineData("pwwwkew", 3)]
        [InlineData("aab", 2)]
        public void LongestSubString_HappyPathTest_WithRepeatingLetters(string input, int output)
        {
            FunStringPlays.LongestSubstring(input).Should().Be(output);
        }

        [Theory]
        [InlineData("au", 2)]
        [InlineData(" ", 1)]
        public void LongestSubString_HappyPathTest_WithNoRepeatingLetters(string input, int output)
        {
            FunStringPlays.LongestSubstring(input).Should().Be(output);
        }
        
        [Theory]
        [InlineData("", 0)]
        [InlineData(null, 0)]
        public void LongestSubString_SadPathTest(string input, int output)
        {
            FunStringPlays.LongestSubstring(input).Should().Be(output);
        }
        
         
        [Theory]
        [InlineData("aba", 0)]
        [InlineData("abcdefg", -1)]
        [InlineData("ppqq", 2)]
        public void MinSwapsForPalindromeTest(string input, int output)
        {
            MinSwaps.MinSwapsForPalindrome(input).Should().Be(output);
        }
    }
}