using System;
using System.Collections.Generic;
using FluentAssertions;
using FluentAssertions.Execution;
using FunCoding.Src.MultiThreading;
using FunCoding.UserDetails;
using Xunit;

namespace FunCoding.Test.MultiThreadingTests
{
    public class MergeListTests
    {
        [Fact]
        private void WhenVariableUserListIsPassed_MergeThemInOrderOneItemPerList()
        {
            var happyPathData1 = new List<IUser>
            {
                new User("Siyavash", "Come here often?", "10"),
                new User("Reza", "SUP???", "290"),
                new User("Frank", "PaidUser", "15"),
                new User("André", "Comment ca-va?", "11"),
            };
            var happyPathData2 = new List<IUser>
            {
                new User("Siyavash", "Come here often?", "10"),
                new User("Reza", "SUP???", "290"),
                new User("Frank", "PaidUser", "15"),
                new User("André", "Comment ca-va?", "11"),
                new User("Dan", "hello world", "12"),
            };

            /*incase the requirement is to pass the argument for GetMergedUsers through UserRepository,
             then we can mock it like this:
            var fakeUserRepo = A.Fake<IUserRepository>();
            A.CallTo(() => fakeUserRepo.GetNewUsers()).Returns(happyList1);
            A.CallTo(() => fakeUserRepo.GetPaidUsers()).Returns(happyList2);
             */

            var AllUserHappyPathList = new List<IList<IUser>>();
            AllUserHappyPathList.Add(happyPathData1);
            AllUserHappyPathList.Add(happyPathData2);

            var mergedList = MergeLists.GetMergedUsers();
            using (new AssertionScope())
            {
                mergedList.Count.Should().Be(happyPathData1.Count + happyPathData2.Count);
                for (var i = 0; i < mergedList.Count - 1; i += 2)
                    mergedList[i].Should().BeEquivalentTo(mergedList[i + 1]);
            }
        }
        
        [Fact]
        private void WhenNoUserListIsPassed_EmptyListIsReturned()
        {
            var mergedList = MergeLists.GetMergedUsers();
            mergedList.Should().BeEmpty();
        }

        [Fact]
        private void WhenNullIsPassed_NullIsReturned()
        {
            Action act = () => MergeLists.GetMergedUsers();
            act.Should().ThrowExactly<ArgumentNullException>();
        }
    }
}