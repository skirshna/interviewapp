
using FunCoding.Src.ArrayManipulations;
using Xunit;

namespace FunCoding.Test.RomanNumeralTests.Unit
{
    public class RomanNumeralTest
    {
        [Fact]
        private void passRomanNumeral_happyPath_addition()
        {
            RomanNumeral r = RomanNumeral.GetInstance();
            var result = r.RomanNumeralToNumber("XIII");
            Assert.Equal(13, result);
        }
        
        [Fact]
        private void passRomanNumeral_happyPath_subtraction()
        {
            RomanNumeral r = RomanNumeral.GetInstance();
            var result = r.RomanNumeralToNumber("MCMXCIV");
            Assert.Equal(1994, result);
        }
        
        [Fact]
        private void passRomanNumeral_sadPath()
        {
            RomanNumeral r = RomanNumeral.GetInstance();
            var result =r.RomanNumeralToNumber("LCMIG");
            Assert.Equal(-1, result);
        }
    }
}