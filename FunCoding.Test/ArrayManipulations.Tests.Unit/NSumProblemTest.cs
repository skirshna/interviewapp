using System.Collections.Generic;
using FunCoding.Src;
using Xunit;

namespace FunCoding.Test.RomanNumeralTests.Unit
{
    public class NSumProblemTest
    {
        [Fact]
        public void WhenValidInputIsPassedForThreeSum_ShouldReturnAllSubsetThatAddsToTarget()
        {
            int[] nums = new[] {9, 0, 5, 8, 6, 0, 2, 5, 2};
            int threeSumTarget = 15;
            NSumProblem nsum = new NSumProblem();
            
            var output = new List<IList<int>>();
            output.Add(new List<int>(){0,6,9});
            output.Add(new List<int>(){2,5,8});
            
            Assert.Equal(output, nsum.ThreeSum(nums, threeSumTarget));
            
        }
        
        [Fact]
        public void WhenValidInputIsPassedForTwoSum_ShouldReturnAllSubsetThatAddsToTarget()
        {
            int[] nums = {0,1,1,1, 2, 3, 5};
            int TwoSumTarget = 5;
            NSumProblem nsum = new NSumProblem();
            
            var output = new List<IList<int>>();
            output.Add(new List<int>(){0,5});
            output.Add(new List<int>(){2,3});
            
            Assert.Equal(output, nsum.TwoSum(nums, TwoSumTarget));
            
        }
    }
}